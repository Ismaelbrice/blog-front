
export interface Players {
    id?: number,
    name: string,
    image: string,
    position: string,
    id_team : number,
    team: {
        name: string
    }
};

export interface Teams {
    id?: number,
    name: string,
    description : string,
    image: string
};

export interface User {
    id?:number;
    email:string;
    password?:string;
    role?:string;
};


