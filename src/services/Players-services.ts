
import type { Players, Teams, User } from '@/entities'
import axios from 'axios'

export async function fetchPlayers() {
  const response = await axios.get<Players[]>('http://localhost:8080/api/players')
  return response.data
}


export async function fetchTeams() {
  const response = await axios.get<Teams[]>('http://localhost:8080/api/team')
  return response.data
}


export async function fetchOneTeams(id:any) {
  const response = await axios.get<Teams>('http://localhost:8080/api/team/'+id);
  return response.data;
}


export async function postCom(team:Teams) {
  const response = await axios.post<Teams>('http://localhost:8080/api/team', team);
  return response.data;
}


export async function deleteTeam(id:any) {
  await axios.delete<void>('http://localhost:8080/api/team/'+id);
}


export async function updateTeam(team:Teams) {
  const response = await axios.put<Teams>('http://localhost:8080/api/team/'+team.id, team);
  return response.data;
}





