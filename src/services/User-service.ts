import type { User } from "@/entities";
import axios from "axios";



export async function postRegister(user:User) {
    const response = await axios.post<User>('/api/user', user);
    return response.data
}