import { createRouter, createWebHistory } from "vue-router";
import Home from '@/views/Home.vue';
import Teams from '@/views/Teams.vue';
import Players from '@/views/Players.vue';
import TeamCard from "@/views/TeamCard.vue";
import LoginView from "@/views/LoginView.vue";
import Inscription from "@/views/Inscription.vue";

export const router = createRouter({
  history : createWebHistory(),
  routes : [
    {path: '/' , component : Home},
    {path: '/Teams' , component : Teams},
    {path: '/Players' , component : Players},
    {path: '/TeamCard/:id' , component : TeamCard},
    {path: '/login' , component : LoginView},
    {path: '/inscription' , component : Inscription},
  ]
});